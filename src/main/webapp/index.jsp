<html lang="en" class="">
<head>
<meta charset="UTF-8">
<meta name="robots" content="noindex">
<link rel="canonical" href="https://codepen.io/jaycbrf/pen/iBszr/">
<link rel="stylesheet prefetch" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet prefetch" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css">
<style class="cp-pen-styles">#success_message{ display: none;}</style>

</head>

</body>
<div class="container">

<form class="well form-horizontal bv-form" action=" " method="post"  id="contact_form" novalidate="novalidate">
<fieldset>

<!-- Form Name -->
<center><h2><legend>Health Insurance Premium Quote generator</legend></h2></center>

<!-- Text input-->

<div class="form-group">
  <label class="col-md-4 control-label">Name</label>  
  <div class="col-md-4 inputGroupContainer">
  <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input  name="name" placeholder="Name" class="form-control"  type="text">
    </div>
  </div>
</div>

<!-- radio checks -->
 <div class="form-group">
                        <label class="col-md-4 control-label">Gender</label>
                        <div class="col-md-4">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="gender" value="m" /> Male
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="gender" value="f" /> Female
                                </label>
                            </div>
                        </div>
                    </div>

<!-- Text input-->
       
<div class="form-group">
  <label class="col-md-4 control-label">Age</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
  <input type=""number name="age" placeholder="age" class="form-control">
    </div>
  </div>
</div>

<!-- Text input-->
      
<div class="form-group"> 
  <label class="col-md-4 control-label">Hypertension</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
    <select name="hypertension" class="form-control selectpicker" >
      <option value="" >Please select your Option</option>
      <option value="y">Yes</option>
      <option value="n">No</option>
    </select>
  </div>
</div>
</div> 

<!-- Text input-->
 <div class="form-group"> 
  <label class="col-md-4 control-label">Blood_Suger</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
    <select name="blood_suger" class="form-control selectpicker" >
      <option value=" " >Please select your Option</option>
      <option value="y">Yes</option>
      <option value="n">No</option>
    </select>
  </div>
</div>
</div> 

<!-- Text input-->
 <div class="form-group"> 
  <label class="col-md-4 control-label">Overweight</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
    <select name="overweight" class="form-control selectpicker" >
      <option value=" " >Please select your Option</option>
      <option value="y">Yes</option>
      <option value="n">No</option>
    </select>
  </div>
</div>
</div>

<!-- Text input-->
 <div class="form-group"> 
  <label class="col-md-4 control-label">Alcohal</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
    <select name="alcohal" class="form-control selectpicker" >
      <option value=" " >Please select your Option</option>
      <option value="y">Yes</option>
      <option value="n">No</option>
    </select>
  </div>
</div>
</div>

<!-- Text input-->
 <div class="form-group"> 
  <label class="col-md-4 control-label">DailyExercise</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
    <select name="dailyexercise" class="form-control selectpicker" >
      <option value=" " >Please select your Option</option>
      <option value="y">Yes</option>
      <option value="n">No</option>
    </select>
  </div>
</div>
</div>

<!-- Text input-->
 <div class="form-group"> 
  <label class="col-md-4 control-label">Drugs</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
    <select name="drugs" class="form-control selectpicker" >
      <option value=" " >Please select your Option</option>
      <option value="y">Yes</option>
      <option value="n">No</option>
    </select>
  </div>
</div>
</div>

<!-- Text input-->
 <div class="form-group"> 
  <label class="col-md-4 control-label">Blood_Presure</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
    <select name="blood_presure" class="form-control selectpicker" >
      <option value=" " >Please select your Option</option>
      <option value="y">Yes</option>
      <option value="n">No</option>
    </select>
  </div>
</div>
</div>

<!-- Text input-->
 <div class="form-group"> 
  <label class="col-md-4 control-label">Smoking</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
    <select name="smoking" class="form-control selectpicker" >
      <option value=" " >Please select your Option</option>
      <option value="y">Yes</option>
      <option value="n">No</option>
    </select>
  </div>
</div>
</div>

<!-- Success message -->
<div class="alert alert-success" role="alert" id="success_message" style="text-align:center">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>
<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label"></label>
  <div class="col-md-4">
    <button type="submit" class="btn btn-warning" id ="submit">Send <span class="glyphicon glyphicon-send"></span></button>
  </div>
</div>
<script src="http://production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>

<script>
 $(document).ready(function() {
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Please supply your name'
                    }
                }
            },
            gender: {
                validators: {
                        notEmpty: {
                        message: 'Please select your gender'
                    }
                }
            },
            age: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your age'
                    }
                }
            },
            
            hypertension: {
                validators: {
                    notEmpty: {
                        message: 'Please select your response'
                    }
                }
            },
			blood_suger: {
                validators: {
                    notEmpty: {
                        message: 'Please select your response'
                    }
                }
            },
			overweight: {
                validators: {
                    notEmpty: {
                        message: 'Please select your response'
                    }
                }
            },
			alcohal: {
                validators: {
                    notEmpty: {
                        message: 'Please select your response'
                    }
                }
            },
			dailyexercise: {
                validators: {
                    notEmpty: {
                        message: 'Please select your response'
                    }
                }
            },
			drugs: {
                validators: {
                    notEmpty: {
                        message: 'Please select your response'
                    }
                }
            },
			blood_presure: {
                validators: {
                    notEmpty: {
                        message: 'Please select your response'
                    }
                }
            },
			smoking: {
                validators: {
                    notEmpty: {
                        message: 'Please select your response'
                    }
                }
            }
            
            }
        })
        /* .on('success.form.bv', function(e) {
           $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
              $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
             $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
    }); */
});

$("#submit").click(function() {
      
	            //  var bv = $form.data('contact_form');

	  var name = document.getElementsByName("name")[0].value;
	  	  var age = document.getElementsByName("age")[0].value;

		  	  var gender = document.getElementsByName("gender")[0].value;

			  	  var hypertension = document.getElementsByName("hypertension")[0].value;

				  	  var blood_suger = document.getElementsByName("blood_suger")[0].value;

					  	  var overweight = document.getElementsByName("overweight")[0].value;

						  	  var alcohal = document.getElementsByName("alcohal")[0].value;

							  	  var dailyexercise = document.getElementsByName("dailyexercise")[0].value;
								   var drugs = document.getElementsByName("drugs")[0].value;

							var blood_presure = document.getElementsByName("blood_presure")[0].value;
							var smoking = document.getElementsByName("smoking")[0].value;

							
							var jsonbody={
								
								"name":name,
								"gender":gender,
								"age":age,
								"hypertension":hypertension,
								"blod_suger":blood_suger,
								"overweight":overweight,
								"alcohal":alcohal,
								"dailyexercise":dailyexercise,
								"smoking":smoking,
								"drugs":drugs,
								"blod_presure":blood_presure
								
							}


	  
         $.ajax({ 
         	 headers: { 
        	'Accept': 'application/json',
       		 'Content-Type': 'application/json' 
    			},
             type: "post",
			 dataType: "json",
             data:JSON.stringify(jsonbody),
             url: "http://localhost:2525/health-insurance-premium-quote-generator/QuoteHandler/getQuote",
             success: function(data){        
              // alert(data);		
			//console.log(data.res)	;		   
			   if(data.res) {				   // DO SOMETHING     
					$('#success_message').show();
					$('#success_message').html(data.res);
					} 
					
             }
         });
    });


</script>
</fieldset>
<input type="hidden" value="">
</form>
</div><!-- /.container -->


</body>
</html>	