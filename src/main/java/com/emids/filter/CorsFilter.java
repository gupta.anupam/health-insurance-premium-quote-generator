package com.emids.filter;


import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.OncePerRequestFilter;
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CorsFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
                                    throws ServletException, IOException {
       // response.addHeader("Access-Control-Allow-Origin", "*");
        
        Enumeration headerNames = request.getHeaderNames();
        while(headerNames.hasMoreElements()) {
          String headerName = (String)headerNames.nextElement();
          System.out.println("header name   " + headerName+"   values " + request.getHeader(headerName));
         // System.out.println("values " + request.getHeader(headerName));
        }
        
        Enumeration params = request.getParameterNames(); 
        while(params.hasMoreElements()) {
          String headerName = (String)params.nextElement();
          System.out.println("request name   " + headerName+"   values " + request.getParameter(headerName));
         // System.out.println("values " + request.getHeader(headerName));
        }
        System.out.println("Filtered");
 /*       if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE,OPTIONS");
            response.addHeader("Access-Control-Max-Age", "1800");// 30 min
            response.addHeader("Access-Control-Request-Headers","*");
            //response.setHeader("Access-Control-Allow-Headers", "Origin,Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
            response.setHeader("Access-Control-Allow-Headers", "*");
            response.addHeader("Access-Control-Allow-Origin", "*");
            
        }
 */       
      /*  if (request.getMethod().equals("OPTIONS")) {
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE,OPTIONS");
            response.addHeader("Access-Control-Max-Age", "1800");// 30 min
            response.setHeader("Access-Control-Allow-Headers", "X-ACCESS_TOKEN,Access-Control-Allow-Origin,Authorization,Origin,x-requested-with,Content-Type,Content-Range,Content-Disposition,Content-Description");
        }
        response.addHeader("Access-Control-Allow-Origin", "*");
        
        
        filterChain.doFilter(request, response);*/
        
        HttpServletResponse response1 = (HttpServletResponse) response;	
        	    response1.setHeader("Access-Control-Allow-Origin", "*");
        	    response1.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        	    response1.setHeader("Access-Control-Allow-Headers", "X-ACCESS_TOKEN,Access-Control-Allow-Origin,Authorization,Origin,x-requested-with,Content-Type,Content-Range,Content-Disposition,Content-Description");
        	    response1.setHeader("Access-Control-Max-Age", "3600");
        	    if (!request.getMethod().equals("OPTIONS")) {
        	    	filterChain.doFilter(request, response);
        	    } else {
        	    }
        
    }
}