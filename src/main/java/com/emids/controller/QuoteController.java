package com.emids.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.emids.model.Quoteresponce;
import com.emids.model.UserData;
import com.emids.service.QuoteService;
import com.emids.serviceDAO.QuoteServiceDAO;


@RestController 
@RequestMapping(value = "/QuoteHandler")
public class QuoteController {

	ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
	QuoteServiceDAO currDealsDAO=ctx.getBean("quoteServiceDAO", QuoteServiceDAO.class);
	
	@RequestMapping(value = "/getQuote", method = RequestMethod.POST)
	public Quoteresponce generoteQuote(@RequestBody UserData userData){
		Quoteresponce quoteresponce=new Quoteresponce();
		
		try{
		String res=currDealsDAO.getQuote(userData);
		System.out.println(res);
		quoteresponce.setRes(res);
		
		
		}catch(Exception e){
			
			quoteresponce.setRes("sorry we are not able to calculate ");
		}
		
		
		return quoteresponce;
		
		
		
	}
	
	
}
